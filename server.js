'use strict'
const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.set('view engine', 'ejs');





app.get('/', (req, res) => {
	res.sendFile(__dirname + '/index.html');
	var cursor = db.collection('todos').find({}, {projection:{ _id: 0 }}).toArray((err, result) => {
		console.log(result);
	});
});

app.get('/load', (req, res) => {
	var cursor = db.collection('todos').find({}, {projection:{ _id: 0 }}).toArray((err, result) => {
		res.send(result);
	});
});

// Add new todo
app.post('/add', (req, res) => {
	console.log(req.body.todo);
  	db.collection('todos').save({todo: req.body.todo, done: false}, (err, result) => {
    if (err) return console.log(err)
    	res.send('Added to DB');
  });
})

// Delete todo
app.post('/delete', (req, res) => {
	let task = req.body.todo;
	console.log(task);
	db.collection('todos').findOneAndDelete({todo: task}, (err, result) => {
    if (err) return console.log(err) 
    	res.send('Deleted from DB');
  })
})

// MongoDB connect + PORT listener
const MongoClient = require('mongodb').MongoClient
var db
MongoClient.connect('mongodb://mainuser:mainuser1@ds111390.mlab.com:11390/tododb', (err, client) => {
	if (err) return console.log(err)
	db = client.db('tododb') // database name here
	// db.createCollection('todos')
	app.listen(3000, () => {
	console.log('listening on 3000')
	})
});




